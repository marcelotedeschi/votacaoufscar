<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Votação UFSCar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <?php
  include 'dbinfo.php';

  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);

  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  $sql = "SELECT COUNT(ra) AS 'count' FROM aluno WHERE voto=0";
  $result = $conn->query($sql);

  $row = $result->fetch_assoc();
  $contra = $row['count'];

  $sql2 = "SELECT COUNT(ra) AS 'count' FROM aluno WHERE voto=1";
  $result = $conn->query($sql2);
  $row = $result->fetch_assoc();
  $afavor = $row['count'];
  ?>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Votação UFSCar</h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li class="active"><a href="index.php">Página Inicial</a></li>
                  <li><a href="create.php">Votar</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <div class="inner cover">
            <h1 class="cover-heading">Bem vindo a página de votação UFSCar</h1>
            <p class="lead">Aqui você pode votar legitimamente a favor ou contra a paralização das aulas!</p>
              <p>A ideia é gerar um documento com nome, ra e voto de cada participante para ser levado em consideração na assembleia
              que ocorrerá na segunda-feira (23/05)</p>
            <p class="lead">
              <a href="create.php" class="btn btn-lg btn-default">Votar</a>
            </p>
          </div>
            <br><br>
          <h2>Resultado da votação:</h2>
            <h3>A Favor: <?php echo $afavor;?></h3>
            <h3>Contra: <?php echo $contra;?></h3>

          <div class="mastfoot">
            <div class="inner">
              <p>Created by <a href="https://marcelotedeschi.com" target="_blank">Marcelo T</a>, using <a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  </body>
</html>

