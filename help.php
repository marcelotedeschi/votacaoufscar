<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Votação UFSCar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">Votação UFSCar</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li><a href="index.php">Página Inicial</a></li>
                            <li><a href="create.php">Votar</a></li>
                            <li class="active"><a href="">Ajuda</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <p>1. Acesse o site do Siga:
                <a target="_blank" href="https://sistemas.ufscar.br/siga/">Link</a>
            </p>
            <p>2. Faça o login com suas credenciais</p>

            <p>3. Clique na aba aluno</p>

            <p>4. Clique em Matrícula</p>

            <p>5. Clique no número de sua matricula</p>

            <p>6. Clique em Emissão de Documentos</p>

            <p>7. Clique em Atestado de Matrícula</p>

            <p>8. Salve o documento em alguma pasta do seu computador</p>

            <p>9. Pronto! Agora basta voltar para a página de voto, clicar em Escolher Arquivo
                (Choose File), e selecionar o documento que voce salvou no passo anterior</p>

            <div class="mastfoot">
                <div class="inner">
                    <p>Created by <a href="https://marcelotedeschi.com" target="_blank">Marcelo T</a>, using <a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>

