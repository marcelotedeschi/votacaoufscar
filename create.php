<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Votação UFSCar</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">Votação UFSCar</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li><a href="index.php">Página Inicial</a></li>
                            <li class="active"><a href="create.php">Votar</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <h3>Favor inserir os dados corretamente pois todos serão verificados! Envie o atestado de matricula que se encontra
            na página do Siga:
                <a target="_blank" href="https://sistemas.ufscar.br/siga/">Link </a>
                <a target="_blank" href="help.php">(Ajuda</a>)</h3>
            <div class="inner cover row">
                <div class="col-lg-6 col-lg-offset-3">
                    <form action="save.php" method="post" class="form-horizontal" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome</label>
                            <input type="text" class="form-control" name="nome" id="exampleInputEmail1" placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Registro do Aluno (RA)</label>
                            <input type="text" class="form-control" name="ra" id="exampleInputEmail1" placeholder="Ra">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Curso</label>
                            <input type="text" class="form-control" name="curso" id="exampleInputEmail1" placeholder="Curso">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Upload de verificação de vínculo com a UFSCar</label>
                            <input type="file" class="form-control" name="comprovante" id="exampleInputEmail1" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Voto</label>
                            <p>(a favor ou contra a paralisação das aulas)</p>
                            <select class="form-control" name="voto" required>
                                <option value="0">Sou Contra</option>
                                <option value="1">Sou a Favor</option>
                            </select>
                        </div>
                        <br>
                        <br>


                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p>Created by <a href="https://marcelotedeschi.com" target="_blank">Marcelo T</a>, using <a href="http://getbootstrap.com" target="_blank">Bootstrap</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>

